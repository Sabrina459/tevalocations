﻿
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;


public class AssetLoader : MonoBehaviour {
  //  [SerializeField] protected BarIndicator barIndicator;
    private void Start() {
        StartCoroutine(InstantiateObject());
    }

    IEnumerator InstantiateObject()
    {
        string uri = Application.streamingAssetsPath + "/scene1" ; 
        Debug.LogWarning(uri);
        var request 
            = UnityWebRequestAssetBundle.GetAssetBundle(uri, 0);
        var operation =  request.SendWebRequest();
        while (!operation.isDone)
        {
            /* 
             * as BugFinder metnioned in the comments
             * what you want to track is uwr.downloadProgress
             */
          //  barIndicator.SetProgress(1, request.downloadProgress);
            yield return null;
        }

        AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);
        GameObject root = bundle.LoadAsset<GameObject>("ClinicRoot");
        Instantiate(root);
        Destroy(gameObject);
    }

    private void Run()
    {
        Vector3 dir = transform.right * Input.GetAxis("Horizontal");
        transform.position = Vector3.MoveTowards(transform.position, transform.position + dir, 3 * Time.deltaTime);
    }
}
