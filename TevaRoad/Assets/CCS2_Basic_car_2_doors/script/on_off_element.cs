﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class on_off_element : MonoBehaviour
{
    public KeyCode keyboard1;

    void Update()
    {

        if (Input.GetKeyDown(keyboard1))
        {
            // show
            // renderer.enabled = true;
            gameObject.GetComponent<Renderer>().enabled = !gameObject.GetComponent<Renderer>().enabled;
        }
   
    }
}
