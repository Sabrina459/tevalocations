using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShutDown : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    AudioSource Car;
    ScreenFader fade;
    public AudioClip Skid;
    public AudioClip Horn;
    AudioSource Camera;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Car = GameObject.FindGameObjectWithTag("Car").GetComponent<AudioSource>();
        Camera = GameObject.FindGameObjectWithTag("FirstPlan").GetComponent<AudioSource>();

        fade = GameObject.Find("Fade").GetComponent<ScreenFader>();
        fade.fadeState = ScreenFader.FadeState.In;
        Car.Stop();
        Camera.Stop();
        Camera.clip = Horn;
        Camera.time = 3f;
        Camera.Play();

        

    }
    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Car.clip = Skid;
        Car.Play();
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
